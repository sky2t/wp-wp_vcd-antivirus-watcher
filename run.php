<?php
require_once('db.php');
class scan extends db
{
	private  $root = '';
	private  $ds = DIRECTORY_SEPARATOR;

	public function __construct() {
		$this->root = str_replace(basename(dirname(__FILE__)), "", __DIR__);

        parent::__construct();
        $this->logWpNames();
//        echo __METHOD__;

        $this->funcFiles();
    }

    /**
     * main function
     */
    public function funcFiles()
    {
        foreach ($this->getAllThemes() as $dir => $theme) {

            foreach ($theme as $th) {
                if(!file_exists($this->root.$dir.$this->DS(['wp-content', 'themes']).$th.$this->ds.$this->DS(['functions.php']))){
                    continue;
                }
                $file = $this->root.$dir.$this->DS(['wp-content', 'themes']).$th.$this->DS(['functions.php']);
//                print_r($dir."\n");
//                print_r($th."\n");
//                print_r(filesize($file)."\n");



//               echo htmlspecialchars(base64_decode(base64_encode(file_get_contents($file))))."\n";
//                $log = fopen(__DIR__ . $this->ds . 'test.txt', "w") or die("Unable to open file!");
//                fwrite($log, file_get_contents($file));
//                fclose($log);
//                print_r($file."\n");


               $fc = $this->fileCheck([
                    'dir' => $dir,
                    'th_name' => $th,
                    'size' => filesize($file),
                    'body' => base64_encode(file_get_contents($file))
                ]);
               
               switch ($fc) {
                   case 1:
//                       echo 'ok';
                       break;
                   case 'added':
                       $this->log($dir.'->'.$th.' Added');
//                       echo 'добавлено';
                       break;

                  // file has been modified | return body code
                   default:
//                       print_r($file."\n");
                       $file = fopen($file, "w") or die("Unable to open file!");
                       fwrite($file, base64_decode($fc));
                       fclose($file);

                       $this->log($dir.'->'.$th.' was rewritten!!!');

                       $this->delFiles($dir);

               }// switch


            } // foreach
        } // foreach

    }

    /**
     * find and delete spy files
     */
    public function delFiles($dir)
    {
        if(file_exists($this->root.$dir.$this->DS(['wp-includes', 'wp-tmp.php']))){
            unlink($this->root.$dir.$this->DS(['wp-includes', 'wp-tmp.php']));
            $this->log('File wp-tmp.php was deleted from '.$dir);
        }

    }

    /**
     * return themes directories
     * @return array
     */
    public function getAllThemes()
    {
        $result = [];
        foreach ($this->getWp() as $dir) {
            $themes = scandir($this->root.$dir.$this->DS(['wp-content', 'themes']));
            unset($themes[array_search('.', $themes, true)]);
            unset($themes[array_search('..', $themes, true)]);
            
            $thdir = [];
            foreach ($themes as $theme) {
                if(!is_dir($this->root.$dir.$this->DS(['wp-content', 'themes']).$theme)){
                    continue;
                }
                $thdir[] = $theme;
            }

            $result[$dir] = $thdir;
        }

        return $result;
    }


    /**
     * all Wp dir names
     * @return array
     */
    public function getWp()
    {
        $wpname = [];
        foreach ($this->getDirNames() as $wp){
            if (file_exists($this->root.$wp.$this->DS(['wp-content', 'themes']))) {
                $wpname[] = $wp;
            }
        }
        return $wpname;
    }

    /**
     * logging working process
     */
    public function log($text)
    {
        $log = fopen(__DIR__ . $this->ds . 'log.txt', "a") or die("Unable to open file!");
        fwrite($log, $text.' '.date("d:m:Y H:m:s", time())."\n");
        fclose($log);
    }

    /**
     * logging Wp dir names
     */
    public function logWpNames()
    {
        $file = __DIR__ . $this->ds . 'wp.txt';
        if (!file_exists($file) || filemtime($file) > time() + 60 * 5 ) {

            $names = '';
            foreach ($this->getWp() as $name) {
                $names .= $name . "\n";
            }

            if (strlen($names) > 0) {
                $log = fopen($file, "w") or die("Unable to open file!");
                fwrite($log, $names);
                fclose($log);
            }

        }
    }

    /**
     * all dir names
     * @return array
     */
    public function getDirNames()
    {
        $dom = scandir($this->root);
        unset($dom[array_search('.', $dom, true)]);
        unset($dom[array_search('..', $dom, true)]);
        $domains = [];
        foreach($dom as $d){
            if(is_dir($this->root.'/'.$d)) $domains[] = $d;

        }
        return $domains;
    }


    public function DS($text = null)
    {
        if($text && is_array($text)){
            $result = '';
            foreach ($text as $tx) {
                $result .= $this->ds . $tx;
                if(substr($tx, -4, 1) === '.'){
                $file = true;
                }
            }
            if(!$file){
                $result .= $this->ds;
            }
            return $result;
        }else{
            if(substr($text, -4, 1) === '.'){
                return $this->ds.$text;
            }else{
                return $this->ds.$text.$this->ds;
            }

        }

    }


} //class

$run = new scan();
